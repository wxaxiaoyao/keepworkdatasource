

## template 布局模块
template是一种布局页面的模块

### default 居中布局
使用方式
```
	```@template
	```
```

### leftMain 带左侧栏布局
```
	```@template/leftMain
	```
```


## toc 模块
```
	```
	useMainContent:true    
	```
```
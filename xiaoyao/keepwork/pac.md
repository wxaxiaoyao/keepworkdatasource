
## 备注
1. 接口BaseUrl: /api/wiki/models/

## 登录 注册
> 走KEEPWORK旧逻辑 待验证

## pac网站关联的keepwork站点
> 建立官方的keepwork站点作为数据源 人工创建 => TODO

## 参赛人员表 

### 数据结构
```
websiteId   number 网站ID
username    string 用户名(keepwork)
portrait    string 头像URL
sex       string 性别
realname    string 姓名
email      string 邮箱
QQId        string QQ号
cellphoneId   stirng 手机号
identifyCardId string 身份证号
```

### 接口
1. 提交成员申请
```
POST website_member/submitMemberApply
{
websiteId: 5,
username: "xiaoyao",
sex:"M",
identifyCardId "123456",
....
}
```
2. 获取成员(已通过审核)列表
```
GET  website_member/getByWebsiteId?websiteId=5
```
3. 同意成员申请
```
POST website_member/agreeMemberApply
参数：同提交申请
```
4. 拒绝成员申请
```
POST website_member/deleteMember
{
websiteId:5,
username:xiaoyao
}
POST website_member/deleteById
```
5. 获取申请成员列表
```
GET website_member/getApplyByWebsiteId?websiteId=xx
```
6. 获取单个成员信息
```
GET website_member/getBySiteUsername?websiteId=xx&username="xxx"
```

## 参赛作品表

### 数据结构

```
websiteId  number 网站ID
userId   number 用户Id
username   string 用户名(keepwork)
realname  string 真实姓名
worksName  string 作品名
worksDesc  string 作品简介
worksLogo  string logo URL
worksFlag  number 3 => 公开组  4 => 学生组
worksState string apply => 申请状态  normal => 正常状态
worksUrl   string 作品地址
schoolName string 校名
awords    array [NPL大奖,NPL最佳教程奖 逗号分隔多项]
identifyUrl string 身份证URL
birthdate  string 出生日期
location  string 住址
liveUrl   string 生活照
visitCount number 浏览量
voteCount number 投票数
# todayVoteCount number 今日投票数 yyyy-mm-dd count
commentCount number 评论数
```

### api接口
1. 提交作品申请 `POST website_works/submitWorksApply`
> 参数： 见表结构  websiteId username worksUrl 必填字段
2. 同意作品提交 `POST website_works/agreeWorksApply`
> 参数： websiteId username worksUrl
3. 删除作品 `POST website_works/deleteById`
> 参数: \_id 记录id
4. 投票接口 `POST website_works/vote`
> 参数：websiteId, worksUrl
5. 搜索接口 `POST website_works/search` 
```
POST website_works/search
params: {
  keyword: "搜索词",
  websiteId: "站点ID",
  from: "起始记录",
  size: "每页记录数",
  awords: "奖项过滤 为空则不对此字段过滤",
  worksFlag: "同awords",
  order: { # 字段排序
    createDate: "desc", # desc or asc  创建日期
    visitCount: "desc, # 访问量
    voteCount: "desc", # 投票量
  }
}
# 返回结果
result: {
  total: 5,  # 记录总数
  list:[    # 记录列表 
  {}...
  ]
}
```
6. 获取作品 `GET website_works/getByWorksUrl?websiteId=xx&worksUrl=xxx`
7. 获取个人作品列表(含通过审核和未审核的) `GET website_works/getByUsername?websiteId=xx`
8. 获取个人已同意作品列表 `GET website_works/getNormalByUsername?websiteId=xx`
9. 获取个人申请作品列表 `GET website_works/getApplyByUsername?websiteId=xx`
10. 获取站点的所有通过审核作品列表 `GET website_works/getByWebsiteId?websiteId=xxx&page=1&size=1`
11. 获取站点的所有未审核作品列表 `GET website_works/getApplyByWebsiteId?websiteId=xxx&page=1&size=1`
12. 增加浏览量: `POST website_works/updateVisitCount`
> 参数: websiteId  workdsUrl

## 作品详情页
> 用户页， 用户页如何取作品的相关数据 => TODO
```
GET /api/mod/worldshare/models/worlds/getByWorldsName?userid=xxx&worldsName=xxx
```

## 投票
> 使用缓存控制单人单天投票数量控制 票数直接在作品表累加即可
> 通过投票接口解决

## 作品搜索
```
POST website_works/search
params: {
  keyword: "搜索词",
  websiteId: "站点ID",
  from: "起始记录",
  size: "每页记录数",
  awords: "奖项过滤 为空则不对此字段过滤",
  worksFlag: "同awords",
  order: { # 字段排序
    createDate: "desc", # desc or asc  创建日期
    visitCount: "desc, # 访问量
    voteCount: "desc", # 投票量
  }
}
# 返回结果
result: {
  total: 5,  # 记录总数
  list:[    # 记录列表 
  {}...
  ]
}
```


## 浏览
> keepwork旧逻辑 待验证

## 评论
> keepwork旧逻辑 待验证


## 数据源相关
1. 获取用户数据源列表 `GET site_data_source/getByUsername?username=xxx`
2. 获取默认数据源 遍历数据源列表 找到sitename=__keepwork__的项，做为默认数据源
3. 上传图片使用默认数据源初始化git 上传图片至git库 图片路径前缀: username_images/
4. 作品列表拉取， 选择作品所在站点对应的数据源，初始化git，拉取作品列表， 详情跟big讨论

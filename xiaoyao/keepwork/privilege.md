
## 用户权限
> 基于站点做权限管理， 站点可用组概念批量管理用户权限，也可单独设置用户权限， 
> 权限级别: 禁止访问=>10  读权限=>20  读写权限=>30


## 接口

### 组接口(group)
>数据表:
```
username string 用户名
groupname string 组名
```

- 创建组 `POST group/upsert`
>参数: groupname

- 删除特定组 `POST group/deleteByName`
>参数: groupname

- 删除用户所有组 `POST group/deleteByUsername`
>参数: 

**上述接口均需携带TOKEN**


### 组用户(group_user)
> 数据表
```
username string 用户名
groupname string 组名
memberName string 成员名
# level number 权限级别
```

- 添加组成员 `POST group_user/upsert`
> 参数: groupname, memberName

- 删除组特定成员 `POST group_user/deleteMember`
> 参数: groupname, memberName

- 删除组成员 `POST group_user/deleteGroup`
> 参数: groupname

- 删除用户的所有组成员 `POST group_user/delete`
> 参数:

- 获取成员所有组 `GET group_user/getByMember?memberName=xxx`


### 站点组(site_group)
> 数据表
```
username string 用户名  站点的所属者
sitename string 站点名
groupUsername string 组的所属者用户名
groupname string 用户名
level number 权限级别
```

- 创建站点组 `POST site_group/upsert`
> 参数: sitename, groupUsername, groupname, level

- 删除特定站点组 `POST site_group/deleteByName`
> 参数: sitename, groupUsername, groupname

- 删除站点里所有组 `POST site_group/deleteBySitename`
> 参数: sitename

- 获取特定站点内的所有组 `GET site_group/getBySitename`
> 参数: usernmae, sitename

- 获取用户的所有站点组 `GET site_group/getByUsername?username=xxx`


### 站点用户(site_user)
>数据表
```
username string 站点所属者用户名
sitename string 站点名
groupUsername string 组所属用户名
groupname string 组名
memberName string 成员名
level number 权限级别
```

- 创建站点用户 `POST site_user/upsert`
> 参数: sitename, memberName, level

- 删除站点用户 `POST site_user/deleteByName`
> 参数: sitename, memberName

- 获取用户的站点权限 `GET site_user/api_getSiteLevelByMemberName`
> 参数: username, sitename, memberName
> 返回: level



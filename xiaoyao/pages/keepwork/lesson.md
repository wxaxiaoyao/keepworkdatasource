```@tocs
```

## API
- API返回2xx表示成功 响应数据为api返回数据
- baseURL = http://10.27.3.3:7001/api/v0/(本地环境)  http://api-stage.keepwork.com/lesson/v0/(stage环境) http://api.keepwork.com/lesson/v0/(正式环境)
- 认证token使用keepwork的token

- 默认增删查改(POST|DELETE|UPDATE|GET)都是对用户个人数据操作, 对系统范围资源检索默认提供search接口(get resouces/search) 资源打包默认提供(get resources/detail)
- 非默认接口按归属方式定义,如课程包内课程(packages/:id/lessons 创建-POST 获取-GET) 用户购买(user/:id/subscribe 创建-POST 获取-GET)
- 每张数据表默认包含createdAt(创建时间) updatedAt(更新时间)两个字段 

### http status code
- 2xx 请求成功处理  
- 400 客户端请求不合法 输入参数有误
- 401 未认证
- 404 资源不存在
- 409 资源冲突 
- 500 服务端异常

### 分页,排序,过滤
无特殊声明资源列表接口(GET :resource/index)默认支持如下参数定制查询结果
- x-page number 页号 默认1
- x-per-page number 页大小 默认200
- x-order string 排序方式 格式 field-order-field-order  例: id-asc-updatedAt-desc
- ...fields  表字段过滤 

#### 高级过滤
限于GET方法参数限制比较大(类型全为字符串，数组表达存在问题(axios id=[1,2] =>  id[] = [1,2])待研究解决), 故提供搜索接口(POST :resource/seach)

- 分页 排序 同列表接口
- field-op=value 字段过滤 格式: 字段名-运算符=值  如 id-eq=1 id=1(-op省略则为等于-eq)  

##### 字段过滤运算符列表
- and: {a: 5}           // AND (a = 5)
- or: [{a: 5}, {a: 6}]     // (a = 5 OR a = 6)
- gt: 6,                // > 6
- gte: 6,               // >= 6
- lt 10,               // < 10
- lte: 10,              // <= 10
- ne: 20,               // != 20
- eq: 3,                // = 3
- not: true,            // IS NOT TRUE
- between: [6, 10],     // BETWEEN 6 AND 10
- notBetween: [11, 15], // NOT BETWEEN 11 AND 15
- in: [1, 2],           // IN [1, 2]
- notIn: [1, 2],        // NOT IN [1, 2]
- like: '%hat',         // LIKE '%hat'
- notLike: '%hat'       // NOT LIKE '%hat'


### 课程包

#### 数据表
```
id number 记录id
userId number 创建者userID
subjectId number 科目ID
packageName string 包名 唯一
minAge number 最小年龄
maxAge number 最大年龄
state number 0 - 初始状态  1 - 审核中  2 - 审核成功  3 - 审核失败  4 - 异常态(审核成功后被改坏可不用此状态 用0代替)
intro string 简介
rmb number 购买所需人民币
coin number 购买知识币数
auditAt string 审核通过时间
extra object 可选非查询字段位于此对象， 根据需求自行存用
extra.coverUrl string 包封面
extra.stateInfo string 状态信息 通常填审核不通过或下架原因
```

##### 课程包搜索
> 获取系统课程包使用此接口

```
GET packages/search
```
输入:
- state 通过状态过滤  state=2 获取审核通过课程包
- ...表字段

输出
- count number 总数
- rows array 课程包对象列表

##### 获取热门课程包
```
GET packages/hots
```
输入:
- 无

输出:
- [package] 课程包对象列表

##### 获取老师的授课课程包列表
```
GET packages/teach
```
输入:
- 无

输出:
- [{...package, lastTeachDate:最后一次授课时间}]

##### 获取用户课程包列表
> 获取用户创建的课程包

```
GET packages
```
输入:
- 无

输出:
- count number 总数
- rows array 课程包对象列表

##### 获取指定课程包
```
GET packages/:id
```
输入:
- 无

输出:
- 表对象

##### 获取指定课程包详细信息
```
GET packages/:id/detail
```
输入:
- 无

输出:
- {...表对象, lessons:课程列表, teachedLessons:已教课程id列表,  learnedLessons:已学课程id列表, isSubscribe:boolean是否购买}

##### 创建课程包
```
POST packages
```
输入:
- lessons: array 包含课程的课程id列表 [1,2,3]
- packageName: string 包名 唯一
- subjectId number 科目ID
- minAge: number 最小年龄
- maxAge: number 最大年龄
- intro: string 简介
- rmb number 购买所需人民币
- coin number 购买知识币数
- extra: object 可选非查询字段位于此对象， 根据需求自行存用
- extra.coverUrl: string 包封面

输出:
- 表对象列表

##### 更新课程包
```
PUT packages/:id
```
输入:
- lessons: array 包含课程的课程id列表 [1,2,3]
- packageName: string 包名 唯一
- subjectId number 科目ID
- minAge: number 最小年龄
- maxAge: number 最大年龄
- intro: string 简介
- rmb number 购买所需人民币
- coin number 购买知识币数
- extra: object 可选非查询字段位于此对象， 根据需求自行存用
- extra.coverUrl: string 包封面

输出:
- 无

##### 删除课程包
```
DELETE packages/:id
```
输入:
- 无

输出:
- 无 

##### 课程包审核

```
POST packages/:id/audit
```
输入:
- state number 0 - 初始状态(取消审核)  1 - 审核中(提交审核申请)  

输出:
- 无 

##### 课程包添加课程
```
POST packages/:id/lessons
```
输入:
- lessonId number 课程id
- lessonNo number 课程编号 为空则为count+1

输出:
- 无 

##### 课程包更新课程编号
```
PUT packages/:id/lessons 
```
输入:
- lessonId number 课程id
- lessonNo number 课程编号

输出:
- 无 

##### 课程包删除课程
```
DELETE packages/:id/lessons?lessonId=1
````
输入:
- lessonId number 课程id

输出:
- 无 

##### 获取课程包里课程列表
```
GET packages/:id/lessons
````
输入:
- 无

输出:
- 课程对象列表 参考课程数据表

##### 课程包购买
```
POST packages/:id/subscribe 
```
输入:
- 无

输出:
- 无

##### 课程包是否已经购买
```
GET packages/:id/isSubscribe 
```
输入:
- 无

输出:
- boolean true - 已购买  false - 未购买

##### 课程包人民币购买
1. 在后台建立商品记录 如 商品名称:课程包 第三方物品ID:1 额外必填字段packageId
2. 在后台建立oauth记录 如 应用名称: lessons 回调地址: https://api-stage.keepwork.com/lesson/v0/pays/callback
3. 前端跳转至支付页 例 `https://stage.keepwork.com/wiki/pay?username=xioayao&app_name=lessons&app_goods_id=1&price=1&additional=%7B%22packageId%22%3A1%7D&redirect=http%3A%2F%2Flocalhost%3A7001%2Fapi%2Fv0%2Fpay%2Fcallback`
4. 用户完成支付购买课程包 (https://stage.keepwork.com/wiki/pay?username=xioayao&app_name=lessons&app_goods_id=1&price=1&additional=%7B%22packageId%22%3A1%7D)

### 课程

#### 数据表:
```
id number 记录id
userId number 创建者userID
lessonName string 课程名 
subjectId number 科目ID
url string 关联页面url  唯一
goals string 目标
extra object 可选非查询字段位于此对象， 根据需求自行存用
extra.coverUrl string 封面
extra.videoUrl string 视屏

```

##### 获取用户创建的课程列表
```
GET lessons 
```
输入:
- 无

输出:
- count number 总数
- [{...lesson, packages:[{...package}]}] 课程对象列表 参考课程数据表

- 课程对象列表

##### 获取课程详情
```
GET lessons/:id/detail
```
输入:
- 无

输出:
- object {...表对象, skills:课程技能列表, packages:所属课程包对象列表}

##### 通过课程URL获取课程详情
```
GET lessons/detail?url=xxx
```
输入:
- url string 课程url

输出:
- object {...表对象, skills:课程技能列表, packages:所属课程包对象列表}

##### 获取单一课程
```
GET lessons/:id
```
输入:
- 无

输出:
- object {...表对象, skills:课程技能列表}

##### 创建课程
```
POST lessons 新建，包含技能信息
```
输入:
- skills: array skill对象列表 如：[{id:1, score:90}]
- lessonName: string 课程名 
- subjectId number 科目ID
- goals: string 目标
- url: string 关联页面url 唯一
- extra: object 可选非查询字段位于此对象， 根据需求自行存用
- extra.coverUrl: string 封面
- extra.videoUrl: string 视屏

输出:
- 课程对象

##### 更新课程
```
PUT lessons/:id
```
输入:
- skills: array skill对象列表 如：[{id:1, score:90}]
- lessonName: string 课程名 唯一
- subjectId number 科目ID
- url: string 关联页面url
- goals: string 目标
- extra: object 可选非查询字段位于此对象， 根据需求自行存用
- extra.coverUrl: string 封面
- extra.videoUrl: string 视屏

输出:
- 无

##### 删除课程
```
DELETE lessons/:id 
```
输入:
- 无

输出:
- 无


##### 发布课程
```
POST lessons/:id/contents
```
输入:
- content: string MD文件内容

输出:
- version number 版本号

##### 课程内容
```
GET lessons/:id/contents?version=1
```
输入:
- verison number 课程版本 可选 为空则获取最新版本内容

输出:
- content string 课程内容

##### 获取课程技能
```
GET lessons/:id/skills
```
输入:
- 无

输出:
- [{skillId: 技能id, skillName: 技能名, socre: 数值}]

```
[
{ 
	id: 2,
   userId: 1,
   lessonId: 1,
   skillId: 2,
   score: 8,
   extra: {},
   createdAt: '2018-08-28T07:15:08.000Z',
   updatedAt: '2018-08-28T07:15:08.000Z',
   skillName: '跳舞' 
}
]
```

##### 增加技能
```
POST lessons/:id/skills
```
输入:
- skillId number 技能ID
- score number 技能值

输出:
- 无

##### 移除技能
```
DELETE lessons/:id/skills?skillId=1
```
输入:
- skillId number 技能ID

输出:
- 无

##### 创建课程的学习记录
```
POST lessons/:id/learnRecords
```
输入:
- 学习记录对象

输出
- 学习记录对象

##### 更新课程的学习记录
```
PUT lessons/:id/learnRecords
```
输入:
- 学习记录对象(需带id)

输出
- 无

##### 获取课程的学习记录列表
```
GET lessons/:id/learnRecords
```
输入:
- 无

输出
- 学习记录对象列表


### 科目
#### 数据表
```
id number 记录id
subjectName string 科目名 唯一
enSubjectName string 英文科目名
extra object 可选非查询字段位于此对象， 根据需求自行存用
```
##### 获取所有科目
> 每个科目会包含科目内的技能 

```
GET subjects
GET admins/subjects
```
输入:
- 无

输出:
- 科目对象列表 

##### 创建科目
```
POST admins/subjects
```
输入:
- subjectName string 科目名

输出:
- 科目对象

##### 移除科目
```
DELETE admins/subjects/:id
```
输入:
- 无

输出:
- 无


### 技能
#### 数据表
```
id number 记录ID
skillName string 技能名
enSkillName string 英文技能名
extra object 可选非查询字段位于此对象， 根据需求自行存用
```

##### 获取技能列表
```
GET skills
GET admins/skills
```
输入:
- 无

输出:
- 技能对象列表

##### 创建技能
```
POST admins/skills
```
输入:
- skillName string 技能名
输出:
- 技能对象

##### 删除技能
```
DELETE admins/skills/:id
```
输入:
- 无

输出:
- 无


### 教师key
#### 数据表
```
id number 记录id
userId number 教师id
key string 激活码
state number 状态 0 -- 未使用 1 -- 已使用  2 -- 禁用
extra object
```
##### 教师KEY列表
```
GET admins/teacherCDKeys
```
输入:
- 无

输出:
- count 记录总数
- rows 教师key对象列表

##### 生成激活码
```
POST admins/teacherCDKeys/generate?count=1
```
输入:
- count number 生成数量

输出:
- 记录对象列表

##### 更新激活码(设置记录状态)
```
PUT admins/teacherCDKeys/:id
```
输入:
- state number 

输出
- 无

##### 移除激活码
```
DELETE admins/teacherCDKeys/:id
```
输入:
- 无

输出:
- 无

### 用户
#### 数据表
```
id number 记录Id
username string 用户名 使用keepwork用户名
nickname string 姓名 lesson编辑显示名
bean number 知识豆
coin number 知识币
lockCoin number 可用知识币
identify number 0 - 默认 1 - 学生 2 - 教师 4 - 申请教师
extra: object 业务数据对象
extra.classroomId 此用户处于的课堂
```

##### 获取当前用户
```
GET users
```
输入:
- 无

输出:
- 用户对象


##### 更新用户信息
```
PUT users/:id
```
输入:
- nickname string 昵称 姓名

输出:
- 无


##### 申请成为教师
```
POST users/:id/applyTeacher
```
输入:
- email string 邮箱

输出:
- 无


##### 成为教师
```
POST users/:id/teacher
```
输入:
- key string 激活码
- school string 学校名 可选

输出:
- 无


##### 用户花费知识币和知识豆
```
POST users/expense
```
输入:
- coin number 可选 消耗知识币的数量
- bean number 可选 消耗知识豆的数量
- description string 消费备注

输出:
- 无


##### 获取用户已购买的课程包(购买不包含自建)
```
GET users/:id/subscribes
```
输入:
- packageState 课程包状态 可选  填 2 - 拉取审核通过的课程包

输出:
- arrary [{...课程包对象, teachedLessons:已教课程id列表,  learnedLessons:已学课程id列表, subscribeState:0-1是否购买}]


##### 用户购买课程包(知识币购买)
> 等效接口 packages/:id/subscribe

```
POST users/:id/subscribes
```
输入:
- packageId number 课程包Id

输出:
- 无

##### 用户购买课程包回调(人民币购买)
> 配置keepwork支付的相关的物品表和oauth表记录， 带上指定参数(指明课程包ID)跳转至keepwork支付页面进行支付, 地址示例:https://stage.keepwork.com/wiki/pay?username=xioayao&app_name=lessons&app_goods_id=1&price=1&additional=%7B%22packageId%22%3A1%7D&redirect=http%3A%2F%2Flocalhost%3A7001%2Fapi%2Fv0%2Fpay%2Fcallback

```
POST pays/callback
```
输入:
- username string 用户名
- price number 价格 人民币
- packageId number 课程包id

输出:
- 无

##### 判断用户是否购买课程包
> 等效接口 packages/:id/isSubscribe

```
GET user/:id/isSubscribe
```
输入:
- packageId number 课程包Id

输出:
- boolean true - 已购买  false - 未购买


### 课堂
#### 数据表
课堂classrooms
```
id number 记录id
packageId number 课程包Id(课程所属课程包)
lessonId number 课程id
userId number 教师id
key string 课堂key
state number 课堂状态 0 - 未开始 1 - 上课中 2 - 上课结束
extra object 可选
```

##### 获取课堂列表
```
GET classrooms
```

输入:
- 无

输出:
- 课程记录列表

##### 创建课堂(上课)
```
POST classrooms
```
输入:
- packageId number
- lessonId number 

输出
- 课堂对象
- extra.packageName string 课程名 
- extra.lessonName string 课程名
- extra.lessonGoals: 课程目标 
- extra.lessonNo: 课程编号 


##### 下课
> 下课接口

```
PUT classrooms/:id/dismiss
```
输入:
- 无

输出
- 无


##### 更新课堂信息
```
PUT classrooms/:id
```
输入:
- packageId number
- lessonId number 
- extra object

输出
- 无

##### 获取课堂
```
GET classrooms/:id
```
输入:
- 无

输出:
- 课堂对象

##### 课堂KEY有效性检查
```
GET classrooms/valid?key=xxx
```
输入:
- key string 课堂key

输出:
- boolean 是否有效 true - 有效(上课中)  false - 无效

##### 加入课堂
```
POST classrooms/join 
```
输入
- key string 课堂key

输出
- 学习记录对象

##### 退出课堂
```
POST classrooms/quit
```
输入:
- 无

输出:
- 无

##### 课堂学习记录(报告根据学习记录动态生成)
```
GET classrooms/:id/learnRecords
```
输入
- 无

输出
- 课堂学习记录列表

##### 创建课堂学习记录
```
POST /classrooms/:id/learnRecords
```
输入:
- 学习记录对象

输出:
- 学习记录对象


##### 更新课堂学习记录
```
PUT /classrooms/:id/learnRecords
```
输入:
- array 学习记录对象列表

输出:
- 无

##### 返回当前课堂
```
GET classrooms/current
```
输入:
- 无

输出:
- {...classroom, learnRecordId: 课堂学习记录ID}


### 学习记录
#### 数据表
```
userId number 用户id
packageId number 包id
lessonId number 课程id
classroomId number 课堂id
state number 学习状态  0 - 学习中 1 - 学习完成
extra object 自定义数据
```

##### 获取用户学习记录列表
```
GET learnRecords
```
输入:
- packageId number 课程包id过滤 可选
- leasonId number 课程id过滤 可选

输出
- count number
- rows array 记录列表

##### 获取单条学习记录
```
GET learnRecords/:id
```
输入:
- 无

输出:
- 表对象

##### 创建学习记录(自学课程调用此接口)
```
POST learnRecords
```
输入:
- packageId number 课程所属课程包id
- lessonId number 课程id
- classroomId number 课堂id  自学不填， 课程学习必填
- state number 学习状态  0 - 学习中 1 - 学习完成
- extra object 自定义数据

输出:
- 表对象

##### 更新学习记录
```
PUT learnRecords/:id
```
输入:
- packageId number 课程所属课程包id
- lessonId number 课程id
- classroomId number 课堂id  自学不填， 课程学习必填
- state number 学习状态  0 - 学习中 1 - 学习完成
- extra object 自定义数据

输出:
- 无

##### 领取学习奖励
```
POST learnRecords/:id/reward 
```
输入:
- id number 学习记录id

输出:
- {coin:奖励的知识币数量(已领取或未学习为0), bean: 奖励的知识豆数量}  

##### 是否已领取奖励
```
GET learnRecords/reward?packageId=1&lessonId=1
```
输入:
- packageId number 课程包Id
- lessonId number 课程Id

输出:
- {coin:奖励的知识币数量(0-未领取), bean: 奖励的知识豆数量(0-未领取)} 


### 交易(trades)
|字段|类型|描述|
| :-: | :-: | :-: |
| id | number | 记录id |
| userId | number | 用户Id |
| type | number | 交易类型 0 - 知识豆 1 - 知识币 |
| amount | number | 交易金额 正数为增加 负数为减少 |
| description | string | 交易备注 |
| extra | object | 附加字段 |

#### 交易列表获取
```
GET trades
```
输入:
- type number 可选 知识豆交易请填0

输出:
- array 表对象列表 


### 通用接口（extra）
> 每张均有extra(object)字段 此字段用于记录些无需参与搜索的信息和一些辅助信息, 如： 站点logo, 用户头像url 这类信息类型在可预见的将来都应不会参与检索,然而当这类信息偏多时，设置其值会变得麻烦， 故提供通用的curd去表的该字段值. 下列resourceName=users, packages, lessons, ... 

#### 设置表的extra值
```
POST :resourceName/:id/extra
```
输入:
- id number 记录id
- ...extra  需设置的extra对象

输出:
- 无

#### 更新表的extra值(merge)
```
PUT :resourceName/:id/extra
```
输入:
- id number 记录id
- ...extra  需设置的extra对象

输出:
- 无

#### 获取表的extra值
```
PUT :resourceName/:id/extra
```
输入:
- id number 记录id

输出:
- extra object

#### 清空表的extra值(extra={})
```
DELETE :resourceName/:id/extra
```
输入:
- id number 记录id

输出:
- 无


### 管理员接口

#### 数据表
- 无

##### 资源列表
```
GET admins/:resourceName
```
输入:
- resourceName string 资源 如 Users, Packages 注意首字母大写

输出:
- count 记录总数
- rows array(object) 资源对象列表

##### 单一资源获取
```
GET admins/:resourceName/:id
```
输入:
- id number 资源id
- resourceName string 资源 如 Users, Packages

输出:
- object 资源对象

##### 资源创建
```
POST admins/:resourceName
```
输入:
- resourceName string 资源 如 Users, Packages
- 资源对象表字段相关值

输出:
- object 资源对象

##### 资源更新
```
PUT admins/:resourceName/:id
```
输入:
- id number 资源id
- resourceName string 资源 如 Users, Packages
- 资源对象表字段相关值

输出:
- 无

##### 资源更新
```
PUT admins/:resourceName/:id
```
输入:
- id number 资源id
- resourceName string 资源 如 Users, Packages

输出:
- 无


### 系统功能

#### 发送邮件
```
POST emails
```
输入: 
- to string 接受邮箱
- subject string 邮件主题
- html string 邮件内容

输出:
- boolean 是否成功








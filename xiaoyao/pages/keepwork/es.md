
## ESServer
> 处理GIT文件数据，并转存ES


## API Interface
### 备注
* baseURL: /api/v0/

1. 提交git数据

```
POST gitlab/submitGitData
```
参数
* path string required git文件路径
* action string required 取值added modified removed
* content string required 文件内容

## todo
1. 权限控制瘫痪

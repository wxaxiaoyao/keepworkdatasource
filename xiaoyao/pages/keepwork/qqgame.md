
## QQ游戏大厅接入(支付 & CanvasURL)

### 前置条件
- QQ开放平台注册接入应用(获取appid appkey 应用开发地址host)
- 应用申请支付接入, 并配置发货地址(QQ服务器限制在9001端口, 非QQ服务器限制https 443)， 如： /goods/delivery 


### 设计流程

#### Canvas服务 接口地址及服务端口配置
鉴于应用开发地址只有一个



#### 支付流程
1. 游戏客户端使用内嵌浏览器拉起CanvasURL或直接访问 http://minigame.qq.com/plat/social_hall/app_frame/?appid=1106994032&param=buy 进入主应用
2. 主应用根据param参数，进入相关页面, 目前仅为支付(params=buy) 下述流程按照支付功能书写
3. 进入支付功能页， 展示可购买的物品展示页（以需求而定）
4. 用户点击预购物品(可进入物品详情页，依需求而定) 发送购买请求到后端， 后端提交支付请求到QQServer 返回QQServer返回的url_params
5. 展示支付框,用户点击支付按钮。QQServer回调发货URL， 回调发货URL可写与游戏后端也可写于主应用后端。建议放于主应用后端（下述逻辑按此方式）
6. 进入主应用后端发货逻辑， 验证请求有效性， 通知游戏后端进行游戏发货, 等待游戏后端处理结果
7. 根据游戏后端反馈结果，对QQServer进行发货确定(是否成功发货)


### 参考链接
- [http://open.qqgame.qq.com/wiki/24/35/56/56.html](http://open.qqgame.qq.com/wiki/24/35/56/56.html)


### DEMO
[http://minigame.qq.com/plat/social_hall/app_frame/?appid=1106994032&param=buy](http://minigame.qq.com/plat/social_hall/app_frame/?appid=1106994032&param=buy)
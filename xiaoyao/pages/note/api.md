

## API 封装

### 类型定义
1 number 数字
2 string 单行文本
3 text 多行文本
4 select 关联类型 随其它字段变化 需配置relateProp

#### select 关联类型
1 本地关联  直接配置options即可
2 远程关联  需定义获取options结构

#### 枚举显示
value 显示 label显示  故需两个字段  prop=value  aliasprop=label 优先aliasprop

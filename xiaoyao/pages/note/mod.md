
## 如何编写一个模块

1. mixins mod组件 引入模块公共部分
2. 定义组件属性(模板参数__data_type__ __default_type__)
3. 编写模块逻辑

### 模块分类
> 以数据结构做为分类条件 相类似的数据结构归属同一类模块

### 模块参数定义
__default_data__ 定义默认数据
__data_type__ 定义数据描述

鉴于多种操作及交互每种数据类型值皆为对象, 类型有:

1. 文本类型(text)
值: {text:"我是文本值"}

2. 链接类型(link)
值: {text: "链接文本", link: "链接地址"}

3. 表单类型(form)
值: {field1: "字段1", field2:"字段2" ....}

4. 对象类型(object)

5. 列表类型(list)

### 数据描述字段解析
- label 字段标签名 用显示
- type 字段类型 text link form object list
- items 对象子字段数据描述
- item 列表子字段数据描述

### 示例
#### 文本示例
```
__default_data__: {
  title:{text:"标题"},
  description:{text:"描述"}
}
__data_type__: {
  label:"名称",
  type: "obejct"
  items: {
    title:{
      type:"text",
    },
    description: {
      type:"text",
    },
  }
}
```

#### 链接示例
```
__default_data__: {
	title: {
    text:"链接文本",
    link:"链接地址",
    target:"是否新窗口打开",
   }
}
__data_type__: {
  label:"数据对象名称",
  type:"object",
  items: {
    title: {
      label:"标题",
      type: "link",
    }
  }
}
```

#### 表单
```
__default_data__: {
  title:"标题",
  description:"描述",
}
__data_type__: {
  label:"名称",
  type:"form",
  items: {
    title:{
      lebal:"label",
      type:"text",
    },
    description:{
      lebal:"label",
      type:"text",
    }
  }
}
```

#### 对象
```
__default_data__: {
  obj:{
    title: {text:"标题"},
  }
}
__data_type__: {
  label:"名称",
  type:"object",
  items: {
    obj:{
      lebal:"label",
      type:"object",
      items: {
        title: {
          label:"label",
          type: "text",
        }
      }
    },
  }
}
```

#### 列表
```
__default_data__: {
  objlist:[
  {
    title: {text:"标题"},
  }
  ]
}
__data_type__: {
  label:"名称",
  type:"object",
  items: {
    objlist:{
      lebal:"label",
      type:"list",
      item: {
        label:"label",
        type: "object",
        items: {
          title: {
            label:"title",
            type: "text",
          }
        }
      }
    },
  }
}
```
### 管理员接口

#### 数据表
- 无

##### 资源列表
```
GET admins/:resourceName
```
输入:
- resourceName string 资源 如 Users, Packages 注意首字母大写

输出:
- array(object) 资源对象列表

##### 单一资源获取
```
GET admins/:resourceName/:id
```
输入:
- id number 资源id
- resourceName string 资源 如 Users, Packages

输出:
- object 资源对象

##### 资源创建
```
POST admins/:resourceName
```
输入:
- resourceName string 资源 如 Users, Packages
- 资源对象表字段相关值

输出:
- object 资源对象

##### 资源更新
```
PUT admins/:resourceName/:id
```
输入:
- id number 资源id
- resourceName string 资源 如 Users, Packages
- 资源对象表字段相关值

输出:
- 无

##### 资源更新
```
PUT admins/:resourceName/:id
```
输入:
- id number 资源id
- resourceName string 资源 如 Users, Packages

输出:
- 无
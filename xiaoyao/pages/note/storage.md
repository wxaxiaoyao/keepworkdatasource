
# 文件管理服务

## 需求
- 用户页(md) 
- 用户静态文件(image, video, audio) 
- 文件地址需可读性 => /username/sitename/filename
- 文件权限， 归属于站点(文件来自站点) 归属于用户(来自文件管理器)
- 用户存贮空间大小限制
- 存贮服务可选择， 暂不考虑混用，可切换但不混用
- 私有站点文件主动授权给共有站点使用，以及防止其它伪造用户主动公开行为

## 实现
- 鉴于文件url的可读性及复用性 
> 访问形式: https://statics.keepwork.com/api/v0/site_file/siteFileId    


- 针对存贮服务的选择, 支持切换不支持混用(混用设计太高)
> 具体选择的存贮服务作为一个配置存在配置， 程序在使用存储服务时根据配置项调用相关接口
> 如果支持用户可选择，需要将配置存用户个人配置中(暂不做此实现)

- 针对文件归属站点或用户，以及访问者的放问权
> 存贮服务需支持私有化，根据访问url设计，url前两段为username，sitename，根据此两字段查找站点，存在则归属站点，否则归属用户
> 文件即有归属，访问者的访问权自然是对文件归属的访问权， 详情参考[用户权限章节](/xiaoyao/keepwork/privilege)

- 用户存贮大小及相关文件信息记录
> 采用数据记录用户文件信息， 数据表设计如下:
```
id number 记录id
username string 用户名 因路径携带username，sitename 故表中直接用两字段代替userId, siteId
# sitename string 站点名 不存在文件归属站点 只归属用户  文件不可单独方式， 依附站点访问 访问权跟随站点
filename string 文件名
key string 文件KEY  唯一  推荐使用绝对路径 /xiaoyao_files/xiaoyao/index.md   /xiaoyao/index.jpg  也可使用UUID
size number 文件大小
hash string 文件hash 去重 依据存贮服务不同而计算方式不同 如git sha
public bool 是否为公共文件 默认私有  公有站点上传的文件为公开文件
```
- 私有站点文件主动授权给共有站点使用，以及防止其它伪造用户主动公开行为
> 记录用户的主动公开行为， 用户主动公开私有文件记录用户公开的站定信息， 记录表(site_file)如下
```
id number 站点文件ID
fileId number 文件ID
username string not null 站点的用户名
sitename string not null 站点名
```

## 存贮服务上的文件格式
> 存贮服务上文件组织形式，尽量方便用户导出后进行组织与管理

方式一:
/pages/username/xxxx
/files/username/xxxx
/datas/username/xxxx
方式二:
/username/xxxx
/username_files/xxxx
/username_datas/xxxx
方式一较适合用户集中存贮管理(QINIU)， 方式二较适合用户独立存贮管理(GIT), 由于系统两者皆可存，而导出功能也偏向方式二，故选方式二。

## 场景
- 站点内上传图片
> 生成访问URL: https://statics.keepwork.com/api/v0/site_file/siteFileId

- 站点内通过图片管理器插入图片
> 生成访问URL: https://statics.keepwork.com/api/v0/site_file/siteFileId  
> 这里的siteFileId为新的id不是上传时的id 

## 缺陷
- 访问地址存在不可读字符

## 接口
> baseURL: /api/v0/  若无特别声明默认所有接口都需认证 

### 获取上传TOKEN
请求: `GET files/token`

参数：
- key: string 文件key 参考表定义解释

返回:
- token string 上传token

### 上传文件接口
请求: `POST files`

参数:
- username string 用户名
- sitename string 站点名 没有站点则不填或填__keepork__, 两者选其一 TODO
- filename string 文件名
- key string  文件key 存贮服务的文件名 绝对路径 需以/username/sitename/ 开头
- type string 文件类型 方便文件管理
- size number 文件大小
- hash string 文件哈希

返回:
- 略

### 修改文件信息
请求: `PUT files/fileId`

参数:
- 数据表字段

返回:
- 略

### 删除文件
请求: `DELETE files/fileId`

参数:
- fileId number 文件ID

返回:
- 略

### 获取单一文件记录
请求: `GET files/fileId`

参数:
- fileId number 文件id

返回：
- 数据表记录

### 获取认证用户的文件使用信息
请求: `GET files/info`

参数: 
- userId number 用户ID

返回:
- size number 使用大小
- total number 总大小
- count number 文件数量 

### 获取认证用户文件列表
请求: `POST user/userId/files`

参数：
- type string optional 存在，按文件类型过滤

返回：
- 记录列表



## 文件
- 用户文件 ==> 全站使用
- 站点文件 ==> 站点内部使用  禁止站点相互混用






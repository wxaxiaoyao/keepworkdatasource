

### paracraf上课学习

1. 加入课堂(通过课堂id)

```
POST classrooms/join 
```
输入
- key string 课堂key, 即教师端生成的课堂id

输出
- 学习记录对象 {id: 学习记录id, packageId: 课程包Id, lessonId: 课程id, userId: 用户Id}


2. 获取课堂的课程所学课程内容(通过学习记录的课程id)

```
GET lessons/:id/contents?version=1
```
输入:
- id number 课程id
- verison number 课程版本 可选 为空则获取最新版本内容

输出:
- content string 课程内容(md文本)

3. paracraft 解析md重试题并作答(通过学习记录id提交结果)

```
PUT learnRecords/:id
```
输入:
- id number 学习记录id
- state number 学习状态  0 - 学习中 1 - 学习完成
- extra object 自定义数据  试题及结果存于此,应与web前端格式兼容, 另学生当前世界路径也可存此(自定义一个字段,web前端显示即可),

输出:
- 无


#### md文本格式及学习记录extra格式(试题及答案)


https://qiniu.keepwork.com/o_1cbred4av4lkk0u90cprktnb.jpg?e=1538282567&token=LYZsjH0681n9sWZqCM4E2KmU6DsJOE7CAM4O3eJq:yM416ilaVVQ-2UUdOnfvajUqtp0=;attname=838ba61ea8d3fd1fb0502abc354e251f94ca5fdf.jpg
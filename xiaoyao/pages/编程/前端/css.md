
## CSS

### 文本修饰
>text-decoration 属性主要用于删除链接中的下划线：

实例
```
a:link {text-decoration:none;}
a:visited {text-decoration:none;}
a:hover {text-decoration:underline;}
a:active {text-decoration:underline;}
```
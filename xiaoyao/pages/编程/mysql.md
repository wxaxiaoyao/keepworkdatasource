

### 表复制

1. INSERT INTO SELECT语句

语句形式为：Insert into Table2(field1,field2,...) select value1,value2,... from Table1 [where column =value][]为可选内容要求目标表Table2必须在由于目标表Table2已经存在，所以我们除了插入源表Table1的字段外，还可以插入常量。示例如下：

	insert into tjjrmx(yybh,xh,tjxmbh,jg,sfyx,zhxmbh,tjksbh,jcrq,jcys,ts,ckfw,disporder)
      　　　　　　select '24',xh,tjxmbh,jg,sfyx,zhxmbh,tjksbh,jcrq,jcys,ts,ckfw,disporder from tjjrmx where yybh = 5

2. SELECT INTO FROM语句

	语句形式为：SELECT vale1, value2 into Table2 from Table1

要求目标表Table2不存在，因为在插入时会自动创建表Table2，并将Table1中指定字段数据复制到Table2中。示例如下：

3. 从一个数据库到另一个数据库

语句形式为：insert into 数据库名.框架名.表名(列名) select (列名) from 数据库名.框架名.表名 where 条件
例如：`insert into MyEmp.dbo.tjdjb(yybh) select yybh from MyCmd.dbo.tjdjb where djrq='2009-10-15' and yybh = '11'`


### 表修改
更名 alter table [tablename] rename [newtablename]
添加列 alter table [tablename] add column [colname] [type] [default defauleValue]
更改列 alter table [tablename] change [colname] [newColName] [type] [default defauleValue]
删除列 alter table [tablename] drop column [colname]



### 表数据插入
#### 插入更新
insert into userRanks(userId, createdAt, updatedAt) select id, createdAt, updatedAt from users  ON DUPLICATE KEY UPDATE userId = userId;
#### 插入替换
replace into userRanks(userId, createdAt, updatedAt) select id, createdAt, updatedAt from users; 

### 表数据更新
update userRanks as ur set project = (select count(*) from projects where projects.userId = ur.userId) where id > 0;
## TODO
1. 点击新增文件或目录 出现输入框 自动获取焦点 回车确定 ESC取消  fixed
2. 保存存在延迟， 保存未成功返回前继续编辑应继续处于编辑状态，以显示当前最新内容未保存 
3. 文件旁边的按钮 鼠标悬浮时显示  fixed
4. 没有文件选中以及临时文件处理方式 临时文件可行 不解决 fixed
5. 窗口缩放编辑器自适应 fixed
6. 本地缓存 fixed
7. 删除确认提示 
8. theme 文件支持 单页编辑需不要带theme渲染 添加可选按钮
9. 完善用户页 内容尽量积极促进作用(用户活跃度，最近更新，热度文章)
10. 完善主页
11. 数据源设置默认 不重新拉去列表，造成选中项在变 DB排序问题
12. 编辑器左侧树需单独文件实现 fixed
13. 文件内容本地持久保存 方便快速编辑 维持最近一个月编辑使用的文件  数量控制(一般不会超过)
14. html转md 爬虫抓去下列站点内容：http://www.shouce.ren/ 
15. 自动保存 fixed 默认3分钟间隔 fixed
16. 提供文件列表和文件树两种模式  fixed
17. 消息提示框 alert 组件化实现(指令) fixed
18. 拖拽模块实现(div => md)
19. 工作区拖动增减宽度  fixed
20. 冲突图标应替换文件logo fixed
21. 数据源配置需做缓存(session)  fixed  
22. 记录文件最后编辑位置， 打开时自动设置光标最后编辑位置  选项化  fixed
23. 编辑模式 页面是否渲染theme 选项化
24. git sha本地计算， 保存文件本地计算 减少一次请求 fixed
25. 编辑器及预览垂直滚动条是否去掉
26. 布局模板 不能使用wikipage(独立$scope 需改写成共用的$scope => 采用renderAfter $.html()方式 修复 fixed
27. 模块编辑ID 自加可能不是很好， 无法全局唯一,仅在模块编辑中唯一， 是否需要提取到全局唯一
28. 前端文件打包，减少对静态文件的请求数量  fixed
29. git\_proxy 使用lua webserver框架
30. markdown样式主题 样式主题定制化(尽量避免md文本含有html css)
31. 文件重命名
32. mdconf 与md text值得混合使用 fixed
33. md解析对象html css js支持 fixed
34. 接入vue框架

## vue项目
1. 进入编辑加载indexDB内容 
2. 渲染需优化
3. mod配置化, 用户自己创建模块延后, 内部人员创建系统模块给用户使用
4. 用户不可以DIY模块 模块样式与数据需分离
5. 用户DIY 则样式跟数据捆绑在模块内
6. 动态模块 结合数据表 实现有意义的mod
7. AI自动组件mod
8. tag aliasname 供用户识别
9. 预览滚动
10. 文本输入改善
11. 版本更新页
12. 当前页路径放入url刷新打开当前页  fiexed
13. 拖拽实现 tag tree 利用tree组件拖拽实现  fixed
14. 优化jsonobject编辑  最长子串匹配加文本编辑器

## 总体
1 项目

### 已实现功能
1. 用户注册 登录
2. 用户git数据源配置， 添加第三方可信任的git源
3. 页面编辑器
4. 用户页访问
5. 用户基本配置设置
6. 用户组及权限控制
7. 

页面本身 记录theme 放弃布局模板使用


### 知识整理
1. nginx http proxy_pass https
> http://blog.csdn.net/medwardm/article/details/72621527


[temp](/xiaoyao/temp)






